package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/tanna.dev/jvt.me-examples/private-gitlab-subgroup-go/for-post/v2/pkg/domain"
	"gitlab.com/tanna.dev/jvt.me-examples/private-gitlab-subgroup-go/for-post/v2/pkg/greeting"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Usage: greet Jamie")
	}

	fmt.Println(greeting.Greeting(domain.Person{
		Name: os.Args[1],
	}))
}
