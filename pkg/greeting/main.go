package greeting

import (
	"fmt"

	"gitlab.com/tanna.dev/jvt.me-examples/private-gitlab-subgroup-go/for-post/v2/pkg/domain"
)

func Greeting(p domain.Person) string {
	return fmt.Sprintf("Hello %s", p.Name)
}
